import json

from BusStop import BusStop


class Map:

    def __init__(self, stops=None):
        if stops is None:
            stops = []
        self._stops = stops

    def __str__(self):
        bs_strings = ["  " + stop.__str__() for stop in self._stops]
        return "<Map: \n" + "\n".join(bs_strings) + "\n>"

    def __len__(self):
        return len(self._stops)

    def __getitem__(self, key):
        return self._stops[key]

    @classmethod
    def load(cls, fname):
        # loads from map file
        m = cls()
        print("fname: ", fname, "\t type: ", type(fname))

        with open(fname, 'r') as f:
            json_list = json.load(f)
            for bs in json_list:
                b = BusStop(bs['_id'], bs['_traveltimes'], bs['_displayname'],
                            bs['_usage'])
                m.add_stop(b)

        return m

    def find(self, stop_id):
        # return index of bus stop.
        for n in range(len(self._stops)):
            if self._stops[n].get_id() == stop_id:
                return n
        # stop not in this map!
        return None

    def get_stop_from_id(self, stop_id):
        return self._stops[self.find(stop_id)]

    def get_stops(self):
        return self._stops

    def remove_stop(self, stop_id):
        self._stops.pop(self.find(stop_id))

    def add_stop(self, stop):
        self._stops.append(stop)

    def to_json(self):
        return [x.to_json() for x in self._stops]

    def to_graph(self):
        import networkx as nx
        dg = nx.Graph()

        # add nodes = busstops
        for s in self._stops:
            dg.add_node(s.get_id())

        # add edges = traveltimes. Only add one edge pr. connection
        for s in self._stops:
            for d, t in s.get_traveltimes().items():
                if not dg.has_edge(s.get_id(), d):
                    dg.add_edge(s.get_id(), d, weight=t)
                elif dg[s.get_id()][d]['weight'] == 0:
                    dg[s.get_id()][d]['weight'] = t

        # remove edges pointing to self
        for s in self._stops:
            dg.remove_edge(s.get_id(), s.get_id())

        return dg

    @classmethod
    def test(cls):
        from BusStop import BusStop

        # create stops w/ traveltimes
        bs1 = BusStop(displayname="Nørreport", usage=0.5)
        bs2 = BusStop(displayname="Ryparken", usage=0.1)
        bs3 = BusStop(displayname="Lyngby", usage=0.2,
                      traveltimes={bs1.get_id(): 10, bs2.get_id(): 45})
        bs1.add_traveltime(bs2.get_id(), 144)
        bs1.add_traveltime(bs3.get_id(), 123)

        # create map, check busses exists
        my_map = Map([bs1, bs2, bs3])

        for bus in [bs1, bs2, bs3]:
            if my_map.find(bus.get_id()) is None:
                raise ValueError("Busstop not found in list!")

        # test add / remove methods
        bs4 = BusStop()
        my_map.add_stop(bs4)
        if my_map.find(bs4.get_id()) is None:
            raise ValueError("Bs4 not added to map!")
        my_map.remove_stop(bs4.get_id())
        if my_map.find(bs4.get_id()) is not None:
            raise ValueError("Bs4 was not removed from map!")

        return my_map


if __name__ == "__main__":
    Map.test()
