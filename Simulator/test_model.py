from BusStop import BusStop
from Map import Map
from Route import Route
from Passenger import Passenger
from Scenario import Scenario
from Bus import Bus
from dsbdc import DSBDC

# run tests
for obj in [BusStop, Map, Route, Passenger, Scenario, Bus, DSBDC]:
    print(obj.test())
    print(str(obj) + " test passed\n")
