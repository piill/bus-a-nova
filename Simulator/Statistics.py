from statistics import stdev
from RejseClient import RejseClient


class Statistics():

    def __init__(self, m):
        self.passenger_count = 0
        self.passengers_in_transit = 0
        self.passengers_hop_on = {}
        self.passengers_drop_off = {}

        self.actual_passengers_hop_on = {}
        self.actual_passengers_drop_off = {}

        self._map = m
        self.rc = RejseClient()

    def export(self, filename):
        with open(filename, "w") as f:
            f.write("id, order, ready, depart, arrive, rejseplanen_depart, rejseplanen_arrive\n")
            for p in self.passengers_drop_off.keys():
                f.write(str(p.get_id()) + ", " + str(p.get_order_time()) + ", " + str(p.get_depart_time()) + ", " +
                        str(self.passengers_hop_on[p]) + ", " +  str(self.passengers_drop_off[p]) + ", " +
                        str(self.actual_passengers_hop_on[p]) + ", " + str(self.actual_passengers_drop_off[p]) + "\n")

    def add_passenger_hop_on(self, passenger, time):
        self.passengers_in_transit += 1
        self.passengers_hop_on[passenger] = time

        # Get stuff from rejseplanen
        dep, arr = self.rc.get_travel(passenger)
        self.actual_passengers_hop_on[passenger] = dep
        self.actual_passengers_drop_off[passenger] = arr

    def add_passenger_drop_off(self, passenger, time):
        new = time - self.passengers_hop_on[passenger]
        if new > self.max_travel_time():
            print("##################################")
            print("New max: ", new)
            print(passenger)
            print("##################################")
        self.passengers_drop_off[passenger] = time
        self.passengers_in_transit -= 1

    def number_of_early_pickups(self):
        ll = [t for (p, t) in self.passengers_hop_on.items()
              if p.get_depart() > t]
        return len(ll)

    def get_wait_times(self):
        return [t - p.get_depart_time() for (p, t)
                in self.passengers_hop_on.items()
                if p.get_depart_time() <= t]

    def max_wait(self):
        w = self.get_wait_times()
        if len(w) == 0:
            return 0
        return max(w)

    def average_wait(self):
        w = self.get_wait_times()
        if len(w) > 0:
            return sum(w)/float(len(w))
        return 0

    def wait_sd(self):
        w = self.get_wait_times()
        if len(w) > 2:
            return stdev(w)
        return 0

    def get_travel_times(self):
        travel = []
        for (p, arrive) in self.passengers_drop_off.items():
            travel.append(arrive - self.passengers_hop_on[p])

        return travel

    def max_travel_time(self):
        t = self.get_travel_times()
        if len(t) > 0:
            return max(t)
        return 0

    def average_travel_time(self):
        t = self.get_travel_times()
        if len(t) > 0:
            return sum(t)/float(len(t))
        return 0

    def travel_time_sd(self):
        t = self.get_travel_times()
        if len(t) > 2:
            return stdev(t)
        return 0

    def get_actual_travel_times(self):
        travel = []
        for (p, arrive) in self.actual_passengers_drop_off.items():
            travel.append(arrive - self.actual_passengers_hop_on[p])

        return travel

    def max_actual_travel_time(self):
        t = self.get_actual_travel_times()
        if len(t) > 0:
            return max(t)
        return 0

    def actual_average_travel_time(self):
        t = self.get_actual_travel_times()
        if len(t) > 0:
            return sum(t)/float(len(t))
        return 0

    def actual_travel_time_sd(self):
        t = self.get_actual_travel_times()
        if len(t) > 2:
            return stdev(t)
        return 0
    def add_passenger_count(self, count):
        self.passenger_count += count

    def get_number_of_passengers(self):
        return self.passenger_count

    def get_passengers_in_transit(self):
        return self.passengers_in_transit
