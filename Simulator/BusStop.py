from id_generator import id_generator


class BusStop:
    def __init__(self, id=None, traveltimes=None, displayname=None, usage=0):
        if id is None:
            id = id_generator(25)
        self._id = id
        self._displayname = displayname
        if traveltimes is None:
            traveltimes = {}
        traveltimes[id] = 0
        self._traveltimes = traveltimes
        self._usage = int(usage)

    def __str__(self):
        if self._displayname is None:
            return repr(self)
        else:
            return self._displayname

    def __repr__(self):
        return "<BusStop-'%s'-%s>" % (self._id, self._displayname)

    def __getitem__(self, busstop_id):
        return self._traveltimes[busstop_id]

    def get_displayname(self):
        return self._displayname

    def get_name(self):
        return self._displayname

    def set_displayname(self, dn):
        self._displayname = dn

    def get_dist(self, dest):
        try:
            if(dest.get_id() == self.get_id()):
                return 0
            return self._traveltimes[dest.get_id()]
        except:
            print(self)
            raise

    def get_id(self):
        return self._id

    def get_usage(self):
        return self._usage

    def set_usage(self, u):
        try:
            self._usage = int(u)
        except:
            print("Wrong usage: ", u)
            raise

    def add_traveltime(self, stop_id, dist):
        self._traveltimes[stop_id] = dist

    def get_traveltimes(self):
        return self._traveltimes

    def set_traveltime(self, s, o):
        self._traveltimes[s] = o

    def get_traveltime(self, s):
        return self._traveltimes[s]

    def to_json(self):
        return self.__dict__

    @classmethod
    def test(cls):
        dist1 = 144
        bs1 = BusStop(displayname="Nørreport", usage=0.5)
        bs2 = BusStop()
        bs1.add_traveltime(bs2.get_id(), dist1)
        if bs1.get_dist(bs2) != dist1:
            raise ValueError("Traveltime incorrect!")

        return bs1


if __name__ == "__main__":
    BusStop.test()
