import datetime
from enum import Enum


class EventTag(Enum):

    BUS_ARRIVE = "BUS_ARRIVE"
    PASSENGER_ORDER = "PASSENGER_ORDER"


class SimulationEvent:

    def __init__(self, tag, subject_id, timestamp):
        self._tag = tag
        self._timestamp = timestamp
        self._subject_id = subject_id

    def __str__(self):
        return "%s: %s on %s" % (
                datetime.timedelta(seconds=self.get_timestamp()),
                str(self.get_tag()),
                self.get_id())

    def __repr__(self):
        return str(self)

    def get_timestamp(self):
        return self._timestamp

    def get_id(self):
        return self._subject_id

    def get_tag(self):
        return self._tag
