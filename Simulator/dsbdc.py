# -*- coding: utf-8 -*-o
from math import inf
from Statistics import Statistics


class DSBDC:

    def __init__(self, bus_map, bus_list, system="new"):

        self.passenger_list = []
        self.bus_map = bus_map
        self.bus_list = bus_list
        self.system = system
        self.time = 0
        self.statistics = Statistics(bus_map)

    def update_time(self, new_time):
        self.time = new_time

    def __str__(self):

        str_dsb = "################# DSBDC report ##################"
        str_dsb += "\n\nBus list:"

        for bus in self.bus_list:
            str_dsb += "\n" + bus.__str__() + "current route:" +\
                (bus.route).__str__() + "\n"

        str_dsb += "\n\nPassenger_list:"
        if self.passenger_list:
            for people in self.passenger_list:
                str_dsb += "\t" + people.__str__() + "\n"

        str_dsb += "\n\nMap: \n" + self.bus_map.__str__()
        return "".join(str_dsb)

    def add_passenger(self, passenger):
        """
            triggered by passenger request event
            update the list of passenger

            passenger_request could be either an event or a passenger
            we need to determine that
                                        """
        self.passenger_list.append(passenger)
        self.passenger_list = sorted(self.passenger_list,
                                     key=lambda x: x.get_depart_time())
        # update passenger statistics
        self.statistics.add_passenger_count(1)

        if self.system == "new":
            # old system doesn't re-plan route
            self.plan_route()
        else:
            # old system still means a passenger will take a particular bus
            passenger.assign_bus(
                    self.bus_a_nova(passenger).get_id())
            print("\t HELLOOOOOOOO\n\n")
            self.update_all_bus_status()

    def plan_route(self):
        for p in self.passenger_list:
            if p.get_status() == "Waiting" and p.get_assigned() is None:
                ba = self.closest_bus(p, "Improved")
                if ba is None:
                    self.passenger_list.remove(p)  # remove passenger
                    print("Passenger was removed..")

                else:
                    p.assign_bus(ba.get_id())

                    # Add the needed busstops to bus route
                    got_origin = False
                    got_dest = False
                    calculated = self.time
                    last_stop = ba.get_origin()
                    for s in ba.get_route("all"):
                        calculated += last_stop.get_dist(s)
                        if s is p.get_origin() and calculated > p.get_depart():
                            got_origin = True
                        if s is p.get_dest() and got_origin:
                            got_dest = True
                            break
                        last_stop = s

                    if not got_origin:
                        # Got neither origin or dest
                        ba.add_stops(p.get_origin())
                        ba.add_stops(p.get_dest())
                    elif got_origin and not got_dest:
                        # Got origin but not dest
                        ba.add_stops(p.get_dest())

                    print("Calculated time to dest: ", (calculated - self.time))


        self.update_all_bus_status()

    def update_all_bus_status(self):
        # update status of bus.
        for bus in self.bus_list:
            if len(bus.get_route("all")) == 0:
                bus.set_status("parked")
            else:
                if bus.get_status() == "parked":
                    bus.set_status("got_route")
                else:
                    bus.set_status("transit")

    def already_in_route(self, bus_id, bus_stop):

        if bus_stop in self.find_bus(bus_id).get_route("all"):
            indexes = [i for i, x in
                       enumerate(self.find_bus(bus_id).get_route("all"))
                       if x == bus_stop]

            print(indexes[0])
            return int(indexes[0])
        else:
            return 0

    def bus_arrived(self, bus_id):
        """
            triggered by bus-stop arrival event
            the passengers in transit whose arrival id is the same as the
            current bus stop id get thrown out, those who wait climb in
            the first stop of the route gets removed
                                                                    """
        # look for passenger that stop at current bus location
        bus = self.find_bus(bus_id)
        if(len(bus.get_route("all")) > 0):
            bus.arrive()

        p_on_bus = [p for p in self.passenger_list
                if p.get_status() == bus_id]
        p_drop = [p for p in p_on_bus if p.get_dest() == bus.get_origin()]

        p_waiting = [p for p in self.passenger_list if p.get_status() == "Waiting"
                    and p.get_origin() == bus.get_origin()
                    and p.get_assigned() == bus.get_id()]

        for p in p_drop:
            print("passenger: ", p.get_id(), " drops off at: ", self.time)
            self.passenger_list.remove(p)
            self.statistics.add_passenger_drop_off(p, self.time)

        for p in p_waiting:
            if p.get_depart() <= self.time:
                print("passenger: ", p.get_id(), " climbs in at: ", self.time)
                p.set_status(bus.get_id())
                self.statistics.add_passenger_hop_on(p, self.time)
            else:
                print("P depart is: ", p.get_depart(), " but bus arrived at ", self.time)


        if self.system == "new":
            # old system doesn't re-plan the route
            self.plan_route()
        else:
            # old system takes busstop out and adds it to end
            # the bus origin becomes also the last element of route
            bus.add_stops(bus.get_origin())
            self.update_all_bus_status()  # account for lack of plan route

        return "passenger updated"

    def bus_a_nova(self, passenger):
        min_dist = inf
        close_bus = self.bus_list[0]

        for bus in self.bus_list:
            s_before = bus.get_origin()
            dist = 0

            passenger_origin = passenger.get_origin()
            passenger_dest = passenger.get_dest()
            bus_route = bus.get_route("all")

            # Check if this bus has the passenger origin
            if passenger_origin in bus_route:
                # Check if bus has destination too,
                # and that the bus hasn't passed them already
                br = bus_route[bus_route.get_index(passenger_origin):]
                if passenger_dest in br:
                    stops = bus_route[:bus_route.get_index(passenger_dest)]
                    for s in stops:
                        dist += s_before.get_dist(s)
                # The destination is not in bus route
                # so we might want to add it
                else:
                    for s in bus_route:
                        dist += s_before.get_dist(s)
                        dist += bus.get_route(-1) \
                                   .get_dist(passenger_dest)
            # The origin is not in the bus route, and will not be inserted
            # We never insert bus stops into planned route (only append)
            else:
                for s in bus_route:
                    dist += s_before.get_dist(s)
                    if len(bus_route) > 0:
                        dist += bus.get_route(-1) \
                                   .get_dist(passenger_origin)
                    else:
                        dist += bus.get_origin() \
                                   .get_dist(passenger_origin)

                    dist += passenger_origin.get_dist(passenger_dest)

            if dist < min_dist:
                close_bus = bus
                min_dist = dist

        return close_bus

    def closest_bus(self, passenger, select):
        """
            return the bus closest to the passenger's origin
            select :    "current" the bus location is used
                        "dest" the bus final destination is used
            output : Either a bus or None if no bus can pick up passenger

                                                            """
        min_dist = inf
        close_bus = None  # set it as None if no bus possible it will return None

        for bus in self.bus_list:
            if(select == "current"):
                dist = bus.get_origin().get_dist(passenger.get_origin())
            elif select == "last":
                dist = bus.get_route(-1).get_dist(passenger.get_origin())
            else:
                too_early = 1 # A hack to bypass the other system..
                got_origin = False
                got_dest = False
                calculated = self.time
                last_stop = bus.get_origin()
                for s in bus.get_route("all"):
                    calculated += last_stop.get_dist(s)
                    if not got_origin and s is passenger.get_origin() and passenger.get_depart() <= calculated:
                        got_origin = True
                    if got_origin and s is passenger.get_dest():
                        got_dest = True
                        break
                    last_stop = s

                if not got_origin and calculated >= passenger.get_depart():
                    calculated += last_stop.get_dist(passenger.get_origin())
                    got_origin = True
                    if not got_dest:
                        calculated += passenger.get_origin().get_dist(passenger.get_dest())
                        got_dest = True
                elif not got_dest:
                    calculated += last_stop.get_dist(passenger.get_dest())
                    got_dest = True

                if calculated < min_dist and got_dest:
                    close_bus = bus
                    min_dist = calculated

        return close_bus

    def find_bus(self, bus_id):
        """
            return the bus object of bus_id
                                        """
        # print("bus_id:",bus_id)
        for bus in self.bus_list:
            if bus_id == bus.get_id():
                return bus

    @classmethod
    def test(cls):
        from Route import Route
        from Scenario import Scenario
        from Bus import Bus

        # load test scenario
        sc1 = Scenario.test()

        # test 1: bus with pre-existing route
        bus1 = Bus(origin=sc1.get_map()._stops[0],
                   route=Route(stops=sc1.get_map()._stops))

        dsb1 = DSBDC(sc1.get_map(), [bus1])
        for p in sc1.get_passengers():
            dsb1.add_passenger(p)

        # route should now be p1.orig -> p1.dest -> p2.orig -> p2.dest
        r1 = dsb1.bus_list[0].get_route("all")
        p1 = dsb1.passenger_list[0]
        p2 = dsb1.passenger_list[1]

        if r1[0] != p1.get_origin() or\
                r1[1] != p1.get_dest() or\
                r1[2] != p2.get_origin() or\
                r1[3] != p2.get_dest():
            raise ValueError("wrong route planned test1")
        else:
            print("Test 1: successful\n")

        # test 2: bus with empty route
        p1.status = "Waiting"  # reset passenger
        sc1 = Scenario.test()

        dsb2 = DSBDC(sc1.get_map(), [Bus(origin=sc1.get_map()._stops[0])])
        for p in sc1.get_passengers():
            print("passenger:", p, "\n")
            dsb2.add_passenger(p)

        # route should now be p1.orig -> p1.dest -> p2.orig -> p2.dest
        r1 = dsb2.bus_list[0].get_route("all")
        p1 = dsb2.passenger_list[0]
        p2 = dsb2.passenger_list[1]

        # dsb2.bus_arrived(dsb2.bus_list[0].id)

        if r1[0] != p1.get_origin() or\
                r1[1] != p1.get_dest() or\
                r1[2] != p2.get_origin() or\
                r1[3] != p2.get_dest:
            raise ValueError("wrong route planned test2")
        else:
            print("Test 2: successful\n")

        sc1 = Scenario.test()
        dsb2 = DSBDC(sc1.get_map(), [Bus(origin=sc1.map._stops[0])])

        for p in sc1.passengers:
            dsb2.add_passenger(p)

        dsb2.bus_arrived(dsb2.bus_list[0].get_id())
        dsb2.bus_arrived(dsb2.bus_list[0].get_id())
        dsb2.bus_arrived(dsb2.bus_list[0].get_id())
        dsb2.bus_arrived(dsb2.bus_list[0].get_id())
        dsb2.bus_arrived(dsb2.bus_list[0].get_id())

        # picking up and dropping of
        print("Test 3: successful\n")
        return dsb2


if __name__ == "__main__":
    DSBDC.test()
