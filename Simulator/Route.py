from Map import Map


class Route(Map):

    def __init__(self, stops=None):
        super(Route, self).__init__(stops)

    def __str__(self):
        bs_strings = ["  " + str(x) + ": " + str(self[x])
                      for x in range(len(self._stops))]
        return "<Route: \n" + "\n".join(bs_strings) + "\n>"

    def next_stop(self):
        # remove next stop and return that
        if(len(self._stops) > 0):
            return self._stops.pop(0)
        return None

    def get_index(self, stop_id):
        for i in self._stops:
            if i.get_id == stop_id:
                return i
        return None

    def rm_route_except_next(self):
        del self._stops[1:]

    def to_graph(self):
        import networkx as nx
        dg = nx.DiGraph()

        # add nodes = busstops
        for n in range(len(self)):
            dg.add_node(self[n].get_id())
            if n > 0:
                bs0, bs1 = self[n - 1: n + 1]
                dg.add_edge(bs0.get_id(), bs1.get_id(),
                            weight=bs0[bs1.get_id()])

        return dg

    @classmethod
    def test(cls):
        # run test of route, and return object tested on
        from BusStop import BusStop

        # create route from map
        map1 = Map.test()
        route1 = Route(stops=map1._stops[:])

        # copy - test indexing!
        route2 = Route(stops=route1[:])

        # add one stop
        bsn = BusStop()
        route1.add_stop(bsn)
        r_len = len(route1)
        r_len_des = len(map1) + 1
        if r_len != r_len_des:
            raise ValueError("Route length %d, should be %d" %
                             (r_len, r_len_des))

        # empty route
        route1.rm_route_except_next()
        if len(route1) != 1 or route1[0] != route2[0]:
            raise ValueError("Route length not reduced to 1!")

        route1.next_stop()
        if not isinstance(route1._stops, list):
            raise ValueError("Route 'stops' property no longer a list!")

        return route2


if __name__ == "__main__":
    Route.test()
