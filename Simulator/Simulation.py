from dsbdc import DSBDC
from SimulationEvent import EventTag
from Bus import Bus
from Scenario import Scenario
from SimulationEvent import SimulationEvent
# from Statistics import Statistics


class Simulation:

    def __init__(self, scenario, system="new"):
        # List of future events
        self.events = []
        self.simulation_time = float(0)
        # Create and set up the simulation scenario
        self.scenario = scenario
        # Create a instance of dsbdc using the newly created busses
        self.dsbdc = DSBDC(self.scenario.get_map(), self.scenario.get_busses(),
                           system)

        # Create initial passenger order event for each passenger in scenario
        for p in self.scenario.get_passengers():
            depart_time = p.get_depart_time() + self.simulation_time
            self.add_event(SimulationEvent(EventTag.PASSENGER_ORDER,
                                           p.get_id(), depart_time))

    def handle_next_event(self, verbose=False):
        events_left = len(self.events)

        # If we have 0 events left, the simulation is probably over
        if events_left <= 0:
            print("Simulation over")
            return events_left

        # Get the next event that should happen
        current_event = self.events.pop(0)
        print("\n############# EVENT AT TIME %d #################" % current_event.get_timestamp())
        self.update_time(current_event.get_timestamp())
        self.dsbdc.update_time(current_event.get_timestamp())
        # The current event that should happen is either a:
        # BUS_ARRIVE event
        if(current_event.get_tag() == EventTag.BUS_ARRIVE):
            # Have the bus pick up / drop off passengers
            c_event_id = current_event.get_id()
            bus = self.dsbdc.find_bus(c_event_id)
            print("Bus (", bus.get_id(), ") arrived at ", bus.get_route(0).get_displayname())
            self.dsbdc.bus_arrived(c_event_id)
            # If we (dsbdc) didn't plan the next stop for the bus
            # then the bus is not going to arrive there (no where)
            if(len(bus.get_route("all")) > 0):
                eta = self.estimate_arrival(bus)
                self.add_event(SimulationEvent(EventTag.BUS_ARRIVE,
                                               bus.get_id(), eta))

        # PASSENGER_ORDER event
        elif(current_event.get_tag() == EventTag.PASSENGER_ORDER):
            passenger = self.scenario.find_passenger(current_event.get_id())
            self.dsbdc.add_passenger(passenger)
            bus = self.dsbdc.find_bus(passenger.get_assigned())
            if bus is None:
                print("passenger :", passenger.get_id(), "is too early\n\n")
                self.add_event(SimulationEvent(EventTag.PASSENGER_ORDER,
                                               passenger.get_id(),
                                               current_event.get_timestamp()+100))  # CHANGE TIME HERE
            else:
                eta = self.estimate_arrival(bus)

                print("Passenger (", passenger.get_id(), ") ordered a bus from ",
                      passenger.get_origin().get_displayname(),
                      " to ", passenger.get_dest().get_displayname())

                if bus.get_status() == "got_route":
                    # the bus has been waiting for a bus.route
                    print("The bus was parked, waiting for events")
                    self.add_event(SimulationEvent(EventTag.BUS_ARRIVE,
                                                   bus.get_id(), eta))

        else:
            print("Unknown event tag: " + str(current_event.get_tag()))

        return events_left

    def estimate_arrival(self, bus):
        """ Calculate the time at which the bus will arrive at his next stop"""
        travel_time = bus.get_eta()
        return self.simulation_time + travel_time

    def update_time(self, update):
        self.simulation_time = float(update)

    def has_event(self):
        if(len(self.events) > 0):
            return True
        return False

    def get_events(self):
        return self.events

    def print_event_list(self):
        for event in self.events:
            print(event)

    def add_event(self, event):
        # e_id = event.get_id() + "-" + str(event.get_tag().value)
        self.events.append(event)
        self.events = sorted(self.events, key=lambda x: x.get_timestamp())

    @classmethod
    def test(cls):
        # create scenario from file, with 1 bus
        scenario = Scenario.load("../MSGenerator/midt_syd_scenario.json")

        bus1 = Bus(scenario.get_map().get_stops()[0])
        system = "new"  # set system to "old" to simulate without routeplanning

        if system == "old":
            for stop in scenario.get_map().get_stops():
                # making sure the route includes all bus stops
                bus1.add_stops(stop)
            scenario.add_bus(bus1)
        else:
            scenario.add_bus(Bus(scenario.get_random_bus_stop()))

        sim1 = Simulation(scenario, system)
        # run simulation
        while(True):
            # input()
            sim1.handle_next_event()
            # print("Events left: " + str(events_left))
            if not sim1.has_event():
                break


if __name__ == '__main__':
    Simulation.test()
