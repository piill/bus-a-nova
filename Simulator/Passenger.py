from id_generator import id_generator


class Passenger:

    def __init__(self, origin, destination, depart_time, request_time,
                 id=None, status=None):
        # assign default values
        if id is None:
            id = id_generator(5)
        if status is None:
            status = "Waiting"

        self._id = id
        self._status = status
        self._assigned_bus = None
        self._origin = origin
        self._destination = destination
        self._request_time = request_time
        self._depart_time = depart_time
        self._location = origin

    def __repr__(self):
        return "\n[Passenger %s] %s -> %s\n" \
               "bus: %s\n" \
               "status: %s\n" \
               "depart time: %s\n" \
               "location: %s\n\n" % \
               (self._id, self._origin._id,
                self._destination._id,
                self._assigned_bus,
                self._status,
                self._depart_time,
                self._location)

    def __str__(self):
        return self.get_id()

    def set_status(self, s):
        self._status = s

    def get_status(self):
        return self._status

    def get_id(self):
        return self._id

    def get_origin(self):
        return self._origin

    def get_dest(self):
        return self._destination

    def get_depart(self):
        return self._depart_time;

    def get_assigned(self):
        return self._assigned_bus

    def assign_bus(self, bus_id):
        self._assigned_bus = bus_id

    def to_json(self):
        res = self.__dict__
        res['destination'] = self._destination.to_json()
        res['origin'] = self._origin.to_json()
        res['location'] = self._location.to_json()
        return res

    def get_depart_time(self):
        return self._depart_time

    def get_order_time(self):
        return self._request_time

    @classmethod
    def test(cls):
        from BusStop import BusStop
        p1 = Passenger(origin=BusStop(), destination=BusStop(),
                       depart_time=100, request_time=10)
        return p1


if __name__ == "__main__":
    Passenger.test()
