import unittest
from Simulation import Simulation
from SimulationEvent import SimulationEvent, EventTag
from SimulationScenario import SimulationScenario


class TestSimulation(unittest.TestCase):

    '''def test_add_event(self):
        sim = Simulation(SimulationScenario(), 1)
        self.assertEqual(len(sim.events), 0)
        sim.add_event("")
        self.assertEqual(len(sim.events), 1)
'''
    def test_handle_next_event(self):
        sim = Simulation(SimulationScenario(), 20)
        sim_event = SimulationEvent(EventTag.PASSENGER_ORDER, "123")
        sim.add_event(sim_event)
        # Should return PASSENGER_ORDER event
        self.assertEqual(sim_event, sim.handle_next_event())
        self.assertEqual(len(sim.events), 0)
        
        '''sim = Simulation(SimulationScenario(), 20)
        sim_event = SimulationEvent(EventTag.BUS_ARRIVE, "")
        sim.add_event(sim_event)
        # Should return BUS_ARRIVE event
        self.assertEqual(sim_event, sim.handle_next_event())
        self.assertEqual(len(sim.events), 0)
        '''



if __name__ == '__main__':
    unittest.main()
