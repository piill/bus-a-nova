from id_generator import id_generator
from Route import Route


class Bus:

    def __init__(self, origin, route=None, id=None, speed=1, location=0,
                 status="parked", name=None):

        if id is None:
            id = id_generator(25)
        self._id = id
        self._speed = speed
        self._location = location
        self._origin = origin
        self._status = status
        if route is None:
            route = Route()
        self._route = route
        self._name = name

    def __str__(self):
        dest = ""
        if len(self._route) > 0:
            dest = self._route[0]
        return "Bus '%s': %s -> %s" % (self._id, self._origin, dest)

    def __repr__(self):
        return "<Bus-%s>" % self._id

    def set_status(self, s):
        self._status = s

    def get_status(self):
        return self._status

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_route(self, i="all"):
        if i == "all":
            return self._route
        else:
            return self._route[i]

    def get_origin(self):
        return self._origin

    def get_location(self):
        return self._location

    def move(self, time=0):
        if len(self._route) > 0:
            dist = self._origin.get_dist(self._route[0])
            if(dist == 0):
                self._location += 0
            else:
                self._location += (time * self._speed / dist)

        # bus has arrived !
        if self._location >= 1:
            self.arrive()

    def arrive(self):
        # set location to 0, update route and origin
        self._location = 0
        self._origin = self._route.next_stop()

    def get_id(self):
        return self._id

    def get_eta(self):
        # return estimated time of arrival
        if(len(self._route) > 0):
            return (1 - self._location) * \
                (self._origin.get_dist(self._route[0]) / self._speed)
        return float(0)

    def in_transit(self):
        return self._location > 0

    def update_route(self, new_route):
        self._route = new_route

    def add_stops(self, bus_stop):
        self._route.add_stop(bus_stop)

    def to_graph(self):
        dg = self._route.to_graph()

        # add nodes = busstops
        dg.add_node(self._origin.get_id())
        if len(self._route) > 0:
            dg.add_edge(self._origin.get_id(), self._route[0].get_id(),
                        weight=self._origin[self._route[0].get_id()])
        return dg

    @classmethod
    def test(cls):
        from Route import Route
        route1 = Route.test()
        bus1 = Bus(origin=route1[-1], route=Route(stops=route1[:]))

        # test move func
        eta = bus1.get_eta()
        bus1.move(eta / 2)
        if eta == bus1.get_eta():
            raise ValueError("Bus has not moved!")

        # test arrive
        bus1.arrive()
        if bus1._origin != route1[0] or bus1._route[0] != route1[1]:
            raise ValueError("Arrive failed, bus origin or route not correct")

        return bus1


if __name__ == "__main__":
    Bus.test()
