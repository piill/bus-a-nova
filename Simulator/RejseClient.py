import pycurl
import urllib.parse
import json
import datetime
from io import BytesIO

from Passenger import Passenger # noqa
from BusStop import BusStop # noqa


class RejseClient:
    def __init__(self):
        self.client = pycurl.Curl()
        # For debugging
        # self.client.setopt(self.client.VERBOSE, True)
        self.base_url = 'http://xmlopen.rejseplanen.dk/bin/rest.exe/'

        self.rejse_date = datetime.datetime.today().replace(hour=0,
            minute=0, second=0, microsecond=0)
        self.rejse_date = self.rejse_date + datetime.timedelta(days=1)

        # I am just hoping these are the right ones.
        # To update/search for others use:
        # http://xmlopen.rejseplanen.dk/bin/rest.exe/location?input=beder&format=json
        self.bs_mapping = {'Odder': 727000702,
                           'Beder': 860150301,
                           'Malling': 751151301,
                           'M\u00e5rslet': 860151502,
                           'Solbjerg': 751003702,
                           'Tranbjerg': 860152303,
                           'Ajstrup': 751101302,
                           'Saksild': 727000602,
                           'Norsminde': 727002002}

    def client_get(self, service, parameters):
        url = "%s%s?%s&format=json" % (self.base_url,
                                       service,
                                       urllib.parse.urlencode(parameters))
        # print('\n', url)
        buffr = BytesIO()
        self.client.setopt(self.client.URL, url)
        self.client.setopt(self.client.WRITEDATA, buffr)
        self.client.perform()

        response = buffr.getvalue()
        buffr.close()

        return json.loads(response)

    def get_times_in_seconds(self, t1, t2):
        t1 = datetime.datetime.strptime(t1, '%H:%M')
        t2 = datetime.datetime.strptime(t2, '%H:%M')

        t1_s = (t1 - datetime.datetime(1900, 1, 1)).total_seconds()
        t2_s = (t2 - datetime.datetime(1900, 1, 1)).total_seconds()
        return (t1_s, t2_s)

    def get_trip_lowest_time(self, originId, destId, t_date):
        t_time = datetime.datetime.strftime(t_date, '%H:%M')
        t_date = datetime.datetime.strftime(t_date, '%d.%m.%y')

        params = {'originId': originId,
                  'destId': destId,
                  'useBus': 1,
                  'date': t_date,
                  'time': t_time}

        resp = self.client_get('trip', params)
        trips = resp['TripList']['Trip']
        for trip in trips:
            trip = trip['Leg']

            if type(trip) is list:
                origin = trip[0]['Origin']['time']
                dest = trip[-1]['Destination']['time']
                return self.get_times_in_seconds(origin, dest)
            else:
                origin = trip['Origin']['time']
                dest = trip['Destination']['time']
                return self.get_times_in_seconds(origin, dest)

    def get_travel(self, passenger, travel_date=None):
        # Map bus stop names to rejseplanen ids
        originId = self.bs_mapping[passenger.get_origin().get_displayname()]
        destId = self.bs_mapping[passenger.get_dest().get_displayname()]

        # Check if we should use custom date
        if travel_date is None:
            # Use default date and add the offset from passenger
            r_t = self.rejse_date + datetime.timedelta(
                    seconds=passenger.get_depart())
        else:
            # Use custom date + passenger offset
            travel_date = travel_date.replace(
                hour=0, minute=0, second=0, microsecond=0)
            r_t = travel_date + datetime.timedelta(
                    seconds=passenger.get_depart())

        return self.get_trip_lowest_time(originId, destId, r_t)


if __name__ == "__main__":
    from timeit import default_timer as timer

    # 7051: Hovedbanegården (Tietgensgade)
    # 1161: Rådhuspladsen

    rjc = RejseClient()
    places = ['Odder', 'Beder', 'Malling', 'M\u00e5rslet', 'Solbjerg',
              'Tranbjerg', 'Ajstrup', 'Saksild', 'Norsminde']
    start = timer()
    times = []

    for p1 in places:
        for p2 in places:
            if p1 == p2:
                continue
            passenger = Passenger(
                    origin=BusStop(displayname=p1),
                    destination=BusStop(displayname=p2),
                    depart_time=27000,
                    request_time=90)
            print(p1,
                  "->",
                  p2,
                  rjc.get_travel(passenger, travel_date=None))

    end = timer()
    print("get_travel() x 10: ", round((end-start), 3), "sec.")
