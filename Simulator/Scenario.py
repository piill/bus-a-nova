import random
from functools import reduce
import json

from Passenger import Passenger
from Map import Map
from BusStop import BusStop


class Scenario:

    def __init__(self, map_object, passenger_list, busses=None):
        if busses is None:
            busses = []
        self._busses = busses
        self._map = map_object
        self._passengers = sorted(passenger_list,
                                  key=Passenger.get_depart_time)

    def get_passengers(self):
        return self._passengers

    def get_map(self):
        return self._map

    def get_busses(self):
        return self._busses

    def add_bus(self, bus):
        self._busses.append(bus)

    def set_busses(self, busses):
        self._busses = busses

    def get_bus_stops(self):
        return self._map.get_stops()

    def add_bus_stop(self, bus_stop):
        self._map.add_stop(bus_stop)

    def get_random_bus_stop(self):
        return Scenario._choose(self.get_bus_stops())

    def get_random_bus(self):
        return random.choice(self._busses)

    def find_passenger(self, passenger_id):
        for x in self._passengers:
            if(x.get_id() == passenger_id):
                return x
        return None

    def __str__(self):
        # list passengers w/ depart time
        p_strings = ["  " + str(p.get_depart_time()) + ": " + str(p)
                     for p in self._passengers]
        return ("<Scenario: \n" + str(self._map) + "\n" +
                "Passengers: \n" + "\n".join(p_strings) + "\n>")

    @classmethod
    def generate(cls, bus_stops, timeend, number_of_passengers=None,
                 seed=None, requests_at_zero=False, timestart=0):
        if number_of_passengers is None:
            number_of_passengers = reduce(lambda x, y: x + y,
                                          [x.get_usage() for x in bus_stops])

        if seed is not None:
            random.seed(seed)

        passenger_list = []
        for n in range(number_of_passengers):
            origin = cls._choose(bus_stops)
            destination = cls._choose(bus_stops)
            while destination is origin:
                destination = cls._choose(bus_stops)

            depature_time = random.randint(timestart, timeend)
            request_time = timestart
            if not requests_at_zero:
                request_time = random.randint(timestart, depature_time)

            pas = Passenger(origin, destination, depature_time, request_time)
            passenger_list.append(pas)

        return cls(Map(bus_stops), passenger_list)

    # Choose a random station. Use this instead of random.choices!
    @classmethod
    def _choose(cls, bus_stops):
        w = [x.get_usage() for x in bus_stops]
        total = reduce(lambda x, y: x + y, w)
        acc = w[0]
        for b in bus_stops:
            if random.randint(0, total) <= acc:
                return b
            acc += b.get_usage()

        return bus_stops[-1]

    @classmethod
    def load(cls, fname):
        # loads scenario from file
        m = Map()
        pl = []
        with open(fname, 'r') as f:
            json_scen = json.load(f)

            # map
            for bs in json_scen['map']:
                b = BusStop(bs['_id'], bs['_traveltimes'], bs['_displayname'],
                            bs['_usage'])
                m.add_stop(b)

            # passenger list
            for p in json_scen['passengers']:
                # print(p['origin']['id'])
                origin = m.get_stop_from_id(p['_origin']['_id'])
                dest = m.get_stop_from_id(p['_destination']['_id'])
                pl.append(Passenger(origin, dest, p['_depart_time'],
                                    p['_request_time'], p['_id']))

        return cls(m, pl)

    def to_json(self):
        res = {}
        res['map'] = self._map.to_json()
        res['passengers'] = [x.to_json() for x in self._passengers]
        return res

    @classmethod
    def test(cls):
        from Map import Map
        from Passenger import Passenger

        # initialize map and passengers
        m1 = Map.test()
        p1 = Passenger(origin=m1._stops[0], destination=m1._stops[2],
                       depart_time=20, request_time=10)
        p2 = Passenger(id="pas12", origin=m1._stops[1],
                       destination=m1._stops[0], depart_time=30,
                       request_time=20)

        p3 = Passenger(id="pas13", origin=m1._stops[2],
                       destination=m1._stops[1],
                       depart_time=32, request_time=20)
        # create sceanrio
        sc1 = cls(m1, [p2, p1, p3])

        # check that list is sorted!
        if sc1._passengers[0] != p1:
            raise ValueError("Passengers not sorted correctly!")
        return sc1


if __name__ == "__main__":
    Scenario.test()
