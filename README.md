# Bus-a-nova

## Structure


- `MSGenerator` is the Map & Scenario generator
- `Simulator` contains all the code for the simulator and the model
- `Controller.py` in the root contains all controller code (pr MVC)
- `View` is the main GUI and plotting functions

## Style guide

As a rule of thumb, follow [PEP8](https://www.python.org/dev/peps/pep-0008/#naming-conventions). 
It is not explicitly enforced at all times, but Jespers linter will be sad if not. 

A quick summary:

- Use 4 spaces per indentation level.
- Limit all lines to a maximum of 79 characters.
- Class and file names uses `CapitalizedWords`
- Functions and variables uses `lowercase_with_underscore`
- Use one leading underscore only for non-public methods and instance variables.
- Constants uses `ALL_UPPERCASE`
- Surround top-level function and class definitions with two blank lines.
- Method definitions inside a class are surrounded by a single blank line.
- Imports should be grouped in the following order:
    1. standard library imports
    2. related third party imports
    3. local application/library specific imports
- You should put a blank line between each group of imports.

Additionally, class variables should be private pr default, marked by a leading underscore: `_my_private_var`.
They should instead be called using set/get methods, e.g. `bus1.get_id()` returns `bus1._id`.

