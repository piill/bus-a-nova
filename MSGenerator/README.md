# Map & Scenario generator

# Structure

# Formats

## Bus-stop

A `BusStop`-object contains the following members:

- `display_name`: Non-unique string.
- `usage`: An int indicating how often the stop is used.

Example:
```json
{
    "display_name":"Nørreport st",
    "usage": 382
}
```

## Travel times 

A `TravelTime`-object is a dictionary that maps the keys of bus-stops and the time, in seconds, it takes to travel to that stop. 
It should contain all bus-stops available on the map.

This allows you to easily find the travel distance from `keyA` to `keyB` as such: `travel_time[keyA][keyB]`

Example:
```json
{
    "YgWOCq": "2415",
    "EhzIo1": "1531",
    "ruc4MS": "1531",
}
```

## Map

A `Map`-object contains:

- `bus_stops`: A dictionary that maps unique keys to bus-stops.
- `travel_times`: A dictionary that maps a bus-stop's key and it's corresponding `TravelTime`

Example:
```json
{
    "bus_stops": {
        "qfl2rf": {
            "display_name":"Nørreport st",
            "usage": 382         
            }
        "YgWOCq": {...},
        "EhzIo1": {...},
        "ruc4MS": {...},
    },

    "travel_times": {
        "qfl2rf": {
            "YgWOCq": "2415",
            "EhzIo1": "1531",
            "ruc4MS": "1531",
        }
        "YgWOCq": {...},
        "EhzIo1": {...},
        "ruc4MS": {...},
    }
}
```

## Passenger

A `Passenger`-object contains:

- `start`: The key corresponding to the starting bus-stop
- `end`: The key corresponding to the drop-off bus-stop
- `order_time`: The time the ticket is ordered
- `departure_time`: The time the passenger is ready to begin the journey, this has to be equals to or greater than `order_time`

Example:
```json
{
    "start":"YgWOCq",
    "end":"EhzIo1",
    "order_time": 102,
    "departure_time": 110
}
```

## Scenario

A `Scenario`-object contains:

- `map`: The `Map`-object
- `passengers`: An array of `Passenger`-objects, sorted by `order_time`

# Real life data
