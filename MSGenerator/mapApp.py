#! /usr/bin/python3
import sys
sys.path.append("../Simulator")
from BusStop import *
from Map import *
from Scenario import Scenario
from id_generator import id_generator
from functools import reduce
import json
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


bus_stops = []

class MainGUI(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        hbox = QHBoxLayout()

        # First column
        # Containts the list of busstops
        list_layout = QVBoxLayout()

        self.bs_listWidget = QListWidget()
        self.bs_listWidget.currentRowChanged.connect(self.edit_bus_stop)
        self.add_bus_stop_widget = QPushButton("Add bus stop")
        self.add_bus_stop_widget.clicked.connect(self.add_bus_to_list)

        list_layout.addWidget(self.bs_listWidget)
        list_layout.addWidget(self.add_bus_stop_widget)

        # Second column
        # Contains the busstop details
        details_layout_outer = QVBoxLayout()
        details_layout = QFormLayout()

        # Form elements
        self.name_input = QLineEdit()
        details_layout.addRow("Name:", self.name_input)

        self.usage_input = QLineEdit()
        details_layout.addRow("Usage:", self.usage_input)

        save_edit = QPushButton("Save edit")
        save_edit.clicked.connect(self.edit_finished)
        details_layout.addRow("", save_edit)

        # Travel time
        self.tt_listWidget = QListWidget()
        self.tt_listWidget.itemSelectionChanged.connect(self.edit_travel_time)
        tt_label = QLabel("Travel time:")
        self.tt_input = QLineEdit()
        self.tt_input.textEdited.connect(self.edit_travel_time_done)

        tt_layout = QHBoxLayout()
        tt_layout.addWidget(tt_label)
        tt_layout.addWidget(self.tt_input)

        details_layout_outer.addLayout(details_layout)
        details_layout_outer.addWidget(self.tt_listWidget)
        details_layout_outer.addLayout(tt_layout)

        # Add the columns to the overall layout
        hbox.addLayout(list_layout)
        hbox.addLayout(details_layout_outer)

        window = QWidget()
        window.setLayout(hbox)
        self.init_menubar()
        self.setCentralWidget(window)
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle('Buttons')
        self.show()

    def init_menubar(self):
        menubar = self.menuBar()
        file_menu = menubar.addMenu('&File')

        # Open file
        open_file_action = QAction('&Open', self)
        open_file_action.setStatusTip('Open a mapfile')
        open_file_action.triggered.connect(self.open_file)
        file_menu.addAction(open_file_action)

        # Save file
        save_file_action = QAction('&Save', self)
        save_file_action.setStatusTip('Save a mapfile')
        save_file_action.triggered.connect(self.save_file)
        file_menu.addAction(save_file_action)

        # Generate scenario
        generate_action = QAction('&Generate', self)
        generate_action.triggered.connect(self.gen)
        file_menu.addAction(generate_action)

    def open_file(self):
        (fname, _) = QFileDialog.getOpenFileName(self, 'Open file', './',
                                                 "Map file (*.json *.map)")
        bus_stops.extend(Map.load(fname).get_stops())

        self.update_list()

    def gen(self):
        sgw = scenario_generation_widget(self)
        sgw.show()

    def save_file(self):
        fname = QFileDialog.getSaveFileName(self, 'Save file', './',
                                            "Map file (*.json *.map)")
        with open(fname[0], 'w') as f:
            json_list = [x.__dict__ for x in bus_stops]
            json.dump(json_list, f, sort_keys=True, indent=4)

    def optimize_travel_times(self):
        # Find missing reverse times
        print("Optimize")

    def add_bus_to_list(self):
        if not self.bs_listWidget.findItems("New Item", Qt.MatchExactly):
            bus_stops.append(BusStop(displayname="New Item"))
            item = self.bs_listWidget.item(0)
            self.update_list()
        item = self.bs_listWidget.findItems("New Item", Qt.MatchExactly)[0]
        self.bs_listWidget.setCurrentItem(item)

    def edit_bus_stop(self, current):
        self.name_input.clear()
        self.usage_input.clear()
        bs = bus_stops[current]
        self.name_input.setText(bs.get_displayname())
        self.usage_input.setText(str(bs.get_usage()))

        self.tt_listWidget.clear()
        other_stops = [x.get_displayname() for x in bus_stops]
        self.tt_listWidget.addItems(other_stops)

        self.tt_input.clear()

    def edit_finished(self, _):
        bs = bus_stops[self.bs_listWidget.currentRow()]

        bs.set_displayname(self.name_input.text())
        try:
            bs.set_usage(self.usage_input.text())
        except:
            print("Could not")

        for b in bus_stops:
            for o in bus_stops:
                if o is b:
                    b.set_traveltime(b.get_id(), 0)
                elif o.get_id() not in b.get_traveltimes():
                    if b.get_id() in o.get_traveltimes():
                        b.set_traveltime(o.get_id(), o.get_traveltime(b.get_id()))
                        print("Set to other")
                    else:
                        b.set_traveltime(o.get_id(), 0)
                        print("Set to default")
        self.update_list()

    def update_list(self):
        self.bs_listWidget.clear()
        bs_names = [x.get_displayname() for x in bus_stops]
        self.bs_listWidget.addItems(bs_names)
        self.name_input.clear()
        self.usage_input.clear()

    def edit_travel_time(self):
        print("edit_travel_times")
        bs = bus_stops[self.bs_listWidget.currentRow()]
        os = bus_stops[self.tt_listWidget.currentRow()].get_id()
        if os in bs.get_traveltimes():
            self.tt_input.setText(str(bs.get_traveltime(os)))
        else:
            bs.set_traveltime(os, 0)
            self.tt_input.setText(str(bs.get_traveltime(os)))

    def edit_travel_time_done(self, newText):
        print("edit_travel_times_done")
        bs = bus_stops[self.bs_listWidget.currentRow()]
        os = bus_stops[self.tt_listWidget.currentRow()]
        try:
            bs.set_traveltime(os.get_id(), int(newText))
            os.set_traveltime(bs.get_id(), int(newText))
        except:
            print("could not")


class scenario_generation_widget(QMainWindow):

    def __init__(self, parent=None):
        super(scenario_generation_widget, self).__init__(parent)

        for b in bus_stops:
            for o in bus_stops:
                if o is b:
                    b.set_traveltime(b.get_id(), 0)
                elif o.get_id() not in b.get_traveltimes():
                    if b.get_id() in o.get_traveltimes():
                        b.set_traveltime(o.get_id(), o.get_traveltime(b.get_id()))
                        print("Set to other")
                    else:
                        b.set_traveltime(o.get_id(), 0)
                        print("Set to default")
        self.initUI()

    def initUI(self):
        print("Init ui")
        form_layout = QFormLayout()

        self.np = QLineEdit()
        res = sum([x.get_usage() for x in bus_stops])
        self.np.setText(str(res))
        form_layout.addRow("Number of passengers:", self.np)

        self.timespan = QLineEdit()
        self.timespan.setText(str(60*60*24))
        form_layout.addRow("Timespan (in seconds):", self.timespan)

        self.seed = QLineEdit()
        self.seed.setText(id_generator(25))
        form_layout.addRow("Seed:", self.seed)

        self.request_at_zero = QCheckBox()
        form_layout.addRow("All requests a time 0:", self.request_at_zero)

        gen = QPushButton("Generate")
        gen.clicked.connect(self.generate)
        form_layout.addRow("", gen)

        window = QWidget()
        window.setLayout(form_layout)
        self.setCentralWidget(window)
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle('Buttons')
        self.show()

    def generate(self):
        fname = QFileDialog.getSaveFileName(self, 'Save file', './',"Scenario file (*.json)")
        with open(fname[0], 'w') as f:
            a = Scenario.generate(bus_stops, int(self.timespan.text()), int(self.np.text()), self.seed.text(), self.request_at_zero.isChecked())
            json.dump(a.to_json(), f, sort_keys=True, indent=4, default=lambda o: o.__dict__)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MainGUI()
    sys.exit(app.exec_())
