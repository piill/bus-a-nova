import sys

sys.path.append("./Simulator")
from Scenario import Scenario  # noqa: E402
from Bus import Bus  # noqa: E402
from Simulation import Simulation  # noqa: E402


class Controller:
    def __init__(self):
        self.scenario = None
        self.simulation = None

    def load_scenario(self, file_name):
        self.scenario = Scenario.load(file_name)
        self.simulation = Simulation(self.scenario)

    def run_simulation(self):
        # attempt to create simulation if not exist
        if self.simulation is None:
            self.simulation = Simulation(self.scenario)
        return self.simulation.handle_next_event()

    def add_bus(self, origin_str):
        # check if str repr of bs matches an existing bs.
        # Otherwise choose random origin
        bus_orig = [bs for bs in self.scenario.get_bus_stops() if
                    str(bs) == origin_str]
        if len(bus_orig) > 0:
            self.scenario.add_bus(Bus(bus_orig[0]))
        else:
            self.scenario.add_bus(Bus(self.scenario.get_random_bus_stop()))

    def get_busstop_list(self):
        return [str(bs) for bs in self.scenario.get_map().get_stops()]

    def get_bus_list(self):
        return [str(b) for b in self.scenario.get_busses()]

    def get_bus_plot_info(self, pos):
        names = []
        positions = []
        for b in self.scenario.get_busses():
            # get bus name
            if b.get_name() is None:
                names.append("Bus_%02d" % len(names))
            else:
                names.append(b.get_name())
            # get bus position form its origin
            positions.append(pos[b.get_origin().get_id()])

        return (names, positions)

    def get_event_list(self):
        return [str(e) for e in self.simulation.get_events()]

    def get_statistics(self):
        return self.simulation.dsbdc.statistics

    def get_map_graph(self):
        bs_names = {bs.get_id(): bs.get_name() for bs in
                    self.scenario.get_map().get_stops()}
        return (self.scenario.get_map().to_graph(), bs_names)

    def get_route_graphs(self, pos):
        # build list of route graphs and a list of dicts with route positions
        route_graph = []
        route_pos = []
        for b in self.scenario.get_busses():
            route_graph.append(b.to_graph())
            route_pos.append({bs.get_id(): pos[bs.get_id()] for bs in
                              [b.get_origin()] + b.get_route()[:]})
        return (route_graph, route_pos)
