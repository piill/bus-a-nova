#! /usr/bin/python3
import sys
import argparse

sys.path.append("../Simulator")
sys.path.append("../MSGenerator")
sys.path.append("..")
from BusStop import *
from Scenario import *
from Controller import *

parser = argparse.ArgumentParser(description='Run a simulation.')
parser.add_argument('in', type=argparse.FileType('r'),
    help='Map filename')

parser.add_argument('passengers', type=int,
    help='Number of passengers')

parser.add_argument('buses', type=int,
    help='Number of buses (they all start at the first busstop)')
parser.add_argument('out', type=argparse.FileType('w'),
    help='The file to out')

args = vars(parser.parse_args())
print(args)

bus_stops = Map.load(args['in'].name)
scenario = Scenario.generate(bus_stops, 22 * 3600, args['passengers'], 0, False, 6 * 3600)

controller = Controller()
controller.scenario = scenario

for i in range(args['buses']):
    controller.add_bus(controller.get_busstop_list()[0])

events_left = 1

while events_left > 0:
    events_left = controller.run_simulation()

print(controller.get_statistics().max_wait())
controller.get_statistics().export(args['out'].name)
