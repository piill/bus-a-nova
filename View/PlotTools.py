#! /usr/bin/python3
import re
import networkx as nx
# import matplotlib.pyplot as plt


def plot_map(ax, map_graph, bs_labels):
    # determine positions
    pos = nx.spring_layout(map_graph)

    # plot nodes and node labels
    nx.draw_networkx_nodes(map_graph, ax=ax, pos=pos, hold=True,
                           node_color='b', alpha=0.4, node_size=30)
    label_pos = {key: (p[0], p[1] + 0.03) for key, p in pos.items()}
    label2 = nx.draw_networkx_labels(map_graph, ax=ax, pos=label_pos,
                                     font_weight='normal', font_size=8,
                                     alpha=0.6)
    for key in pos:
        label2[key].set_text(bs_labels[key])

    # edges and edge slabels
    nx.draw_networkx_edges(map_graph, ax=ax, pos=pos, alpha=0.2,
                           style="dashed")
    edge_label = nx.draw_networkx_edge_labels(map_graph, ax=ax, pos=pos,
                                              font_size=5, alpha=0.4)
    # remove excess text
    for key, val in edge_label.items():
        txt = val.get_text()
        val.set_text(re.sub(r"(\{'weight': )|\}", "", txt))

    return pos


def plot_route(ax, route_graph, route_pos):
    lines = nx.draw_networkx_edges(route_graph, pos=route_pos, ax=ax, width=3,
                                   edge_color='g', alpha=0.3, arrows=True)
    return lines


def plot_bus(ax, bus_name, bus_pos):
    bus_text = ax.text(bus_pos[0], bus_pos[1] - 0.04, bus_name,
                       color='g', fontsize=8, fontweight="bold")
    return bus_text


def remove_artists(plot_items):
    for plot in plot_items:
        if plot is not None:
            if isinstance(plot, list):
                remove_artists(plot)
            else:
                plot.remove()
    plot_items.clear()
