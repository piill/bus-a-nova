#! /usr/bin/python3
import sys

from matplotlib.figure import Figure
import matplotlib.pyplot as plt
sys.path.append("../Simulator")
sys.path.append("..")
from PlotTools import *
from Controller import *

if __name__ == "__main__":
    # get map and convert to graph
    c = Controller()
    c.scenario = Scenario.load("../MSGenerator/midt_syd_scenario.json")
    (map_graph, bs_labels) = c.get_map_graph()
    fig = Figure()
    ax = fig.add_axes()
    plot_map(ax, map_graph, bs_labels)
    plt.show()
