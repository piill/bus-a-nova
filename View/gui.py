#! /usr/bin/python3
# Disable pep8 linting for this file.
# It will never be pep8 compatible
# flake8: noqa

import sys
import time

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigCanvas
# import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

sys.path.append("../Simulator")
sys.path.append("../MSGenerator")
sys.path.append("..")
from BusStop import *
from Scenario import *
from Controller import *
from PlotTools import *
import mapApp


class MainGUI(QMainWindow):

    def __init__(self):
        super().__init__()
        self.controller = Controller()
        self.buttons = {}
        self.last_update = 0
        self.max_update_interval = 0
        self.initUI()

        # start timer in background thread
        self.timer = QTimer()
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.run_one)

    def initUI(self):
        hbox = QHBoxLayout()
        hbox.addLayout(self.initLeftUI())
        hbox.addLayout(self.initRightUI())

        window = QWidget()
        window.setLayout(hbox)
        self.init_menubar()
        self.setCentralWidget(window)
        self.setGeometry(0, 0, 900, 600)
        self.setWindowTitle('Simulator')
        for _, b in self.buttons.items():
            b.setEnabled(False)

        self.setWindowIcon(QIcon('fluff/bus.png'))
        self.show()

    def initLeftUI(self):
        vbox = QVBoxLayout()
        self.tab_widget = QTabWidget()
        self.tab_widget.setMinimumWidth(600)

        # Create events tab
        self.events_list = QListWidget()
        self.tab_widget.addTab(self.events_list, "Events")

        # Create busses list + button
        bw = QWidget()
        busLayout = QVBoxLayout()
        self.busses_list = QListWidget()
        add_bus = QPushButton("Add bus")
        add_bus.clicked.connect(self.add_bus)
        self.buttons["add_bus"] = add_bus

        busLayout.addWidget(self.busses_list)
        busLayout.addWidget(add_bus)
        bw.setLayout(busLayout)
        self.tab_widget.addTab(bw, "Busses")

        # plot tab
        self.fig = Figure()
        self.canvas = FigCanvas(self.fig)
        self.ax = self.fig.add_subplot(111)
        self.ax.set_axis_off()
        self.fig.tight_layout()
        self.map_pos = None
        self.plot_items = []
        self.tab_widget.addTab(self.canvas, "Map")

        # final setup
        vbox.addWidget(self.tab_widget)
        return vbox

    def initRightUI(self):
        vbox = QFormLayout()

        run_one = QPushButton("Simulate event")
        run_one.clicked.connect(self.run_one)
        run_one.setMinimumWidth(50)
        vbox.addRow(run_one)
        self.buttons["run_one"] = run_one

        run_all = QPushButton("Start/Stop Simulation")
        run_all.clicked.connect(self.start_stop)
        run_all.setMinimumWidth(50)
        vbox.addRow(run_all)
        self.buttons["run_all"] = run_all

        self.timer_slider = QSlider(Qt.Horizontal)
        self.timer_slider.setRange(1, 100)
        self.timer_slider.setSingleStep(1)
        self.timer_slider.setMinimumWidth(50)
        self.timer_slider.valueChanged.connect(self.slider_update)
        vbox.addRow("Sim speed:", self.timer_slider)

        # add statistics fields
        self.early_pickup_label = QLabel("0")
        vbox.addRow("Early pickups:", self.early_pickup_label)

        self.max_wait_label = QLabel("0")
        vbox.addRow("Max wait:", self.max_wait_label)

        self.avg_wait_label = QLabel("0")
        vbox.addRow("Avg wait:", self.avg_wait_label)

        self.wait_sd_label = QLabel("0")
        vbox.addRow("Sd wait:", self.wait_sd_label)

        self.max_travel_label = QLabel("0")
        vbox.addRow("Max travel:", self.max_travel_label)

        self.avg_travel_label = QLabel("0")
        vbox.addRow("Avg travel:", self.avg_travel_label)

        self.travel_sd_label = QLabel("0")
        vbox.addRow("Sd travel:", self.travel_sd_label)

        self.n_passengers_label = QLabel("0")
        vbox.addRow("Number of passengers:", self.n_passengers_label)

        self.passengers_in_transit_label = QLabel("0")
        vbox.addRow("Passengers in transit", self.passengers_in_transit_label)

        return vbox

    def slider_update(self):
        self.timer.setInterval(5000 / self.timer_slider.value())

    def init_menubar(self):
        menubar = self.menuBar()
        file_menu = menubar.addMenu('&File')

        # Load scenario
        load_file_action = QAction('&Load Scenario', self)
        load_file_action.triggered.connect(self.load_scenario)
        file_menu.addAction(load_file_action)

        # Generate scenario
        scenario_editor_action = QAction('&Scenario Editor', self)
        scenario_editor_action.triggered.connect(self.open_scenario_editor)
        file_menu.addAction(scenario_editor_action)

        # save result
        save_sim_results_action = QAction('&Save sim results', self)
        save_sim_results_action.triggered.connect(self.save_sim_results)
        file_menu.addAction(save_sim_results_action)


    def open_scenario_editor(self):
        ex = mapApp.MainGUI()

    def load_scenario(self):
        (fname, ok) = QFileDialog.getOpenFileName(self, 'Open file', '../MSGenerator/',
                                                  "Map file (*.json *.map)")
        if ok:
            self.controller.load_scenario(fname)
            self.update_view(True)
            self.ax.clear()
            self.ax.set_axis_off()
            map_graph, bs_labels = self.controller.get_map_graph()
            self.map_pos = plot_map(self.ax, map_graph, bs_labels)
            self.canvas.draw()

    def save_sim_results(self):
        fname, ok = QFileDialog.getSaveFileName(self, 'Save file', './', "Result file (*.csv)")
        if ok:
            print(fname)
            self.controller.get_statistics().export(fname)

    def start_stop(self):
        # either start or stop sch.
        if self.timer.isActive():
            self.timer.stop()
        else:
            self.run_one()
            self.timer.start(5000 / self.timer_slider.value())

    def run_one(self):
        self.controller.run_simulation()
        self.update_view()

    def add_bus(self):
        items = ["Random"] + self.controller.get_busstop_list()

        diag_text = "Choose starting location of Bus"
        (item, ok) = QInputDialog.getItem(self, "Add Bus",
                                          diag_text, items, 0, False)

        if ok and item:
            self.controller.add_bus(item)
            self.update_view(True)

    def update_view(self, force=False):
        if force or (time.perf_counter() -
                     self.last_update) > self.max_update_interval:
            # update list of events and busses
            self.busses_list.setUpdatesEnabled(False)
            self.busses_list.clear()
            self.busses_list.setUpdatesEnabled(True)
            self.busses_list.addItems(self.controller.get_bus_list())

            self.events_list.setUpdatesEnabled(False)
            self.events_list.clear()
            self.events_list.setUpdatesEnabled(True)
            self.events_list.addItems(self.controller.get_event_list())
            self.last_update = time.perf_counter()

            # enable/disable buttons
            if self.controller.scenario is not None:
                self.buttons["add_bus"].setEnabled(True)
                if len(self.controller.get_bus_list()) > 0:
                    self.buttons["run_one"].setEnabled(True)
                    self.buttons["run_all"].setEnabled(True)

            # Update statistics
            s = self.controller.get_statistics()
            self.early_pickup_label.setText(str(s.number_of_early_pickups()))
            self.max_wait_label.setText(str(s.max_wait()))
            self.avg_wait_label.setText(str(s.average_wait()))
            self.wait_sd_label.setText(str(s.wait_sd()))
            self.max_travel_label.setText(str(s.max_travel_time()))
            self.avg_travel_label.setText(str(s.average_travel_time()))
            self.travel_sd_label.setText(str(s.travel_time_sd()))
            self.n_passengers_label.setText(str(s.get_number_of_passengers()))
            self.passengers_in_transit_label.setText(str(s.get_passengers_in_transit()))

            # update plot
            if self.controller.scenario is not None and \
                    len(self.controller.get_bus_list()) > 0:
                # draw routes - first delete old ones, then get new
                # routes/route_pos and redraw
                remove_artists(self.plot_items)
                bnl, bpl = self.controller.get_bus_plot_info(self.map_pos)
                rgl, rpl = self.controller.get_route_graphs(self.map_pos)
                for rg, rp, bn, bp in zip(rgl, rpl, bnl, bpl):
                    lines = plot_route(self.ax, rg, rp)
                    bus_text = plot_bus(self.ax, bn, bp)
                    self.plot_items += [lines, bus_text]
                self.canvas.draw()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MainGUI()
    sys.exit(app.exec_())
