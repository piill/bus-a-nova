#! /usr/bin/python3
import sys
import subprocess
import json

import matplotlib.pyplot as plt
from matplotlib.figure import Figure

sys.path.append("../Simulator")
sys.path.append("..")
from Scenario import Scenario
from Map import Map


def run_simulation(map_file, n_pas, n_buses):
    # run sim and save result to file
    for nb in n_buses:
        for np in n_pas:
            # load map and generate scenarios and run simulation
            savename = "results_%04d_passengers_%02d_buses.csv" % (np, nb)
            subprocess.run(["./run_sim.py", map_file, str(np), str(nb), savename])


def generate_scenario(fname, map_file, n_pas, time_end, time_start):
    m = Map.load(map_file)
    with open(fname, 'w') as f:
        sc = Scenario.generate(m.get_stops(), time_end, n_pas[0], 0, False, time_start)
        json.dump(sc.to_json(), f, sort_keys=True, indent=4, default=lambda o: o.__dict__)

# init
n_pas = range(100, 1001, 100)
n_buses = range(2, 6, 1)
sim_end = 22 * 3600
sim_begin = 6 * 3600
map_file = "../MSGenerator/midt_syd_map.json"
savename = ""

run_simulation(map_file, n_pas, n_buses)
